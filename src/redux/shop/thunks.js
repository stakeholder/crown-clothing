import  {
  fetchCollectionsStart,
  fetchCollectionSuccess,
  fetchCollectionFailure
} from "./shop.actions";
import {
  convertCollectionsSnapshotToMap,
  firestore
} from "../../firebase/firebase.utils";


export const fetchCollectionsAsync = () => {
  return (dispatch) => {
    const collectionRef = firestore.collection('collections');
    dispatch(fetchCollectionsStart());
    collectionRef
      .get()
      .then(snapshot => {
      const collectionsMap = convertCollectionsSnapshotToMap(snapshot);
      dispatch(fetchCollectionSuccess(collectionsMap))
      })
      .catch(error => dispatch(fetchCollectionFailure(error.message)));
  };
};
