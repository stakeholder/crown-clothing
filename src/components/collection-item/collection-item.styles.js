import styled, { css } from 'styled-components';
import CustomButton from '../custom-button/custom-button.component';

export const ImageContainer = styled.div`
  width: 100%;
  height: 95%;
  background-size: cover;
  background-position: center;
  margin-bottom: 5px;
  background-image: url(${props => props.imageUrl});
`;

export const CollectionItemContainer = styled.div`
  width: 22vw;
  display: flex;
  flex-direction: column;
  height: 450px;
  align-items: center;
  position: relative;
  &:hover {
    .custom-button {
      opacity: 0.85;
      display: flex;
    }
    .image {
      opacity: 0.8;
    }
  }
`;

export const CollectionFooterContainer = styled.div`
  width: 100%;
  height: 5%;
  display: flex;
  justify-content: space-between;
  font-size: 18px;
`;

export const NameSpanStyle = css`
  width: 90%;
  margin-bottom: 15px;
`;

const getSpanStyle = props => {
  if (props.isName) return NameSpanStyle;
};

export const StyledSpan = styled.span`
  ${getSpanStyle}
`;

export const StyledCustomButton = styled(CustomButton)`
  width: 80%;
  opacity: 0.7;
  display: None;
  position: absolute;
  top: 355px;
`;
