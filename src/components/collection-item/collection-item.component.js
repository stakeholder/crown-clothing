import React from 'react';
import { connect } from 'react-redux';

import { addItem } from '../../redux/cart/cart.actions';

import {
  CollectionItemContainer,
  ImageContainer,
  CollectionFooterContainer,
  StyledSpan,
  StyledCustomButton
} from './collection-item.styles'


const CollectionItem = ({ item, addItem }) => {

  const { name, price, imageUrl } = item;
  
  return (
    <CollectionItemContainer>
      <ImageContainer className='image' imageUrl={imageUrl} />
      <CollectionFooterContainer>
        <StyledSpan isName>{name}</StyledSpan>
        <StyledSpan isPrice>{price}</StyledSpan>
      </CollectionFooterContainer>
      <StyledCustomButton
        className='custom-button'
        onClick={() => addItem(item)} inverted
      >
        Add to cart
      </StyledCustomButton>
    </CollectionItemContainer>
  )
};

const mapDispatchToProps = dispatch => ({
  addItem: item => dispatch(addItem(item))
});

export default connect(null, mapDispatchToProps)(CollectionItem);