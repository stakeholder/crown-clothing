import React from 'react';
import { connect } from 'react-redux';

import { selectCollection } from "../../redux/shop/shop.selector";
import {
  CollectionPageContainer,
  CollectionTitle,
  CollectionItemsContainer,
  StyledCollectionItem
} from "./collections.styles";

const CollectionPage = ({ collection: { items, title } }) => (
  <CollectionPageContainer>
    <CollectionTitle>{title}</CollectionTitle>
    <CollectionItemsContainer>
      {
        items.map(
          item => (<StyledCollectionItem key={item.id} item={item} />)
        )
      }
    </CollectionItemsContainer>
  </CollectionPageContainer>
);

const mapStateToProps = (state, ownProps) => ({
  collection: selectCollection(ownProps.match.params.collectionId)(state)
});

export default connect(mapStateToProps)(CollectionPage);
